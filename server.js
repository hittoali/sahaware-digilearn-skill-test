const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const app = express();
const db = require("./app/models");
require('dotenv').config()
const PORT = process.env.PORT || 8080;
const path = require("path");
const redis = require("redis");

(async () => {

    redisClient = redis.createClient(process.env.REDIS_URL);

    redisClient.on("error", (error) => {
      console.log(error);
    });
    redisClient.on("connect", () => {
      console.log("Redis connected!");
    });

    await redisClient.connect();
})();

const swaggerDefinition = {
    info: {
      title: 'Article API',
      version: '1.0.0',
      description: 'Endpoints to test the user authentication and manipulate article <br><br> <b>Authorize value with `Bearer`, example : Bearer (token) </b>',
    },
    host: process.env.HOST_APP,
    securityDefinitions: {
      bearerAuth: {
        type: 'apiKey',
        name: 'Authorization',
        scheme: 'bearer',
        in: 'header',
        bearerFormat: 'JWT'
      },
    },
  };

const options = {
    swaggerDefinition,
    apis: ['./app/routes/*.js'],
};

const swaggerSpec = swaggerJsDoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));


var corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended:true}));
// db.sequelize.sync();

app.use(express.static(path.join(__dirname, "public")));

require("./app/routes/home.routes")(app);
require("./app/routes/auth.routes")(app);
require("./app/routes/base/permission.routes")(app);
require("./app/routes/base/role.routes")(app);
require("./app/routes/base/menu.routes")(app);
require("./app/routes/base/user.routes")(app);


require("./app/routes/category.routes")(app);
require("./app/routes/article.routes")(app);


app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});