module.exports = (sequelize, Sequelize) => {
    const Article = sequelize.define("article",{
        category_id: {
            type:Sequelize.INTEGER(10)
        },
        title:{
            type:Sequelize.STRING(150)
        },
        short_description: {
            type:Sequelize.TEXT
        },
        description: {
            type:Sequelize.TEXT
        },
        is_visible:{
            type:Sequelize.BOOLEAN
        },
        image: {
            type:Sequelize.STRING
        },
        createdBy: {
            type:Sequelize.INTEGER
        }
    });
    return Article;
}