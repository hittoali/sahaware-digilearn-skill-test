module.exports = (sequelize, Sequelize) => {
    const Permission = sequelize.define("m_permission", {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(150)
        },
        description: {
            type: Sequelize.TEXT
        },
        createdAt : {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        createdBy : {
            type: Sequelize.INTEGER(5)
        },
        updatedAt : {
            type: 'TIMESTAMP'
        },
        updatedBy : {
            type: Sequelize.INTEGER(5)
        },
        deletedAt : {
            type: 'TIMESTAMP'
        },
        deletedBy : {
            type: Sequelize.INTEGER(5)
        }
    });
    return Permission;
}