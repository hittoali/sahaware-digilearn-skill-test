module.exports = (sequelize, Sequelize) => {
    const Menu = sequelize.define("m_menu", {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        parent_id: {
            type: Sequelize.INTEGER(10),
        },
        name: {
            type: Sequelize.STRING(150)
        },
        icon: {
            type: Sequelize.STRING(150)
        },
        url: {
            type: Sequelize.STRING(150)
        },
        position: {
            type: Sequelize.INTEGER(2)
        },
        createdAt : {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        createdBy : {
            type: Sequelize.INTEGER(5)
        },
        updatedAt : {
            type: 'TIMESTAMP'
        },
        updatedBy : {
            type: Sequelize.INTEGER(5)
        },
        deletedAt : {
            type: 'TIMESTAMP'
        },
        deletedBy : {
            type: Sequelize.INTEGER(5)
        }
    });
    return Menu;
}