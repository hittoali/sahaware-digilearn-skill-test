module.exports = (sequelize, Sequelize) => {
    const RoleUser = sequelize.define("mp_role_user", {
        role_id: {
            type: Sequelize.INTEGER(10)
        },
        user_id: {
            type: Sequelize.INTEGER(10)
        },
        createdAt : {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        createdBy : {
            type: Sequelize.INTEGER(5)
        },
        updatedAt : {
            type: 'TIMESTAMP'
        },
        updatedBy : {
            type: Sequelize.INTEGER(5)
        },
        deletedAt : {
            type: 'TIMESTAMP'
        },
        deletedBy : {
            type: Sequelize.INTEGER(5)
        }
    });

    return RoleUser;
}