module.exports = (sequelize, Sequelize) => {
    const RoleMenuPermission = sequelize.define("mp_role_menu_permission", {
        role_id: {
            type: Sequelize.INTEGER(10)
        },
        menu_id: {
            type: Sequelize.INTEGER(10)
        },
        permissions: {
            type: Sequelize.JSON
        },
        createdAt : {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        createdBy : {
            type: Sequelize.INTEGER(5)
        },
        updatedAt : {
            type: 'TIMESTAMP'
        },
        updatedBy : {
            type: Sequelize.INTEGER(5)
        },
        deletedAt : {
            type: 'TIMESTAMP'
        },
        deletedBy : {
            type: Sequelize.INTEGER(5)
        }
    });
    return RoleMenuPermission;
}