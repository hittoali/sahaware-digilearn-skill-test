module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("m_user", {
        id: {
            type: Sequelize.INTEGER(10),
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING(150)
        },
        email: {
            type: Sequelize.STRING(150)
        },
        password: {
            type: Sequelize.STRING
        },
        phone: {
            type: Sequelize.STRING(15)
        },
        expired_token: {
            type: Sequelize.STRING
        },
        reset_token: {
            type: Sequelize.STRING
        },
        createdAt : {
            type: 'TIMESTAMP',
            defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        },
        createdBy : {
            type: Sequelize.INTEGER(5)
        },
        updatedAt : {
            type: 'TIMESTAMP'
        },
        updatedBy : {
            type: Sequelize.INTEGER(5)
        },
        deletedAt : {
            type: 'TIMESTAMP'
        },
        deletedBy : {
            type: Sequelize.INTEGER(5)
        }
    });

    return User;
};