const article = require("../controllers/article.controller");
const { authJwt } = require("../middleware");
const multer = require("multer");
const path = require("path");

const diskStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null,path.join(__dirname,"../../public/image"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            file.fieldname + "-" + Date.now() + path.extname(file.originalname)
            );
    }
});

module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    /**
     * @swagger
     * /api/article/create:
     *  post:
     *      description: Use to post article
     *      tags: [Article APIs]
     *      consumes:
     *        - multipart/form-data
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - name: title
     *          description: Title article
     *          in: formData
     *          type: text
     *        - name: short_description
     *          description: Short description article
     *          in: formData
     *          type: text
     *        - name: description
     *          description: Description article
     *          in: formData
     *          type: text
     *        - name: category_id
     *          description: Category article
     *          in: formData
     *          type: text
     *        - name: image
     *          description: Image article
     *          in: formData
     *          type: file
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.post("/create",multer({ storage: diskStorage }).single('image'),[authJwt.verifyToken],article.create);

    /**
     * @swagger
     * /api/article:
     *  get:
     *      description: Use to get all article
     *      tags: [Article APIs]
     *      consumes:
     *        - application/json
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - name: size
     *          in: query
     *          description: The size of get data
     *          required: false
     *        - name: page
     *          in: query
     *          description: The page of get data
     *          required: false
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.get("",multer().none(),[authJwt.verifyToken],article.getData);

    app.use('/api/article',router);
};