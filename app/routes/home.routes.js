const home = require("../controllers/home.controller");
module.exports = app => {
    var router = require("express").Router();
    
    router.get("",home.home);

    app.use('/',router);
};