const { verifySignUp } = require("../middleware");
const auth = require("../controllers/auth.controller");
const { authJwt } = require("../middleware");

module.exports = function(app) {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    /**
     * @swagger
     * tags:
     *   name: Authentication APIs
     *   description: APIs to handle authentication.
     */

    /**
     * @swagger
     * /api/auth/signup:
     *  post:
     *      description: Use to create user
     *      tags: [Authentication APIs]
     *      consumes:
     *        - application/json
     *      parameters:
     *        - in: body
     *          name: Signup
     *          description: Use to create user
     *          schema:
     *            type: object
     *            required:
     *              - name
     *              - email
     *              - password
     *              - phone
     *            properties:
     *              name:
     *                type: string
     *              email:
     *                type: string
     *              password:
     *                type: string
     *              phone:
     *                type: string
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */

    router.post("/signup", auth.signup);

    /**
     * @swagger
     * /api/auth/signin:
     *  post:
     *      description: Use to login and get token
     *      tags: [Authentication APIs]
     *      consumes:
     *        - application/json
     *      parameters:
     *        - in: body
     *          name: Login
     *          description: Use to login and get token
     *          schema:
     *            type: object
     *            required:
     *              - email
     *              - password
     *            properties:
     *              email:
     *                type: string
     *              password:
     *                type: string
     *      responses:
     *          '200':
     *              description: A successful response
     *          '404':
     *              description: Data not found
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.post("/signin", auth.signin);

    router.post("/logout", [authJwt.verifyToken], auth.logout);

    app.use('/api/auth',router);
};

