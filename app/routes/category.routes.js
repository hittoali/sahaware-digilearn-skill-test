const articleCategory = require("../controllers/category.controller");
const { authJwt } = require("../middleware");
module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    /**
     * @swagger
     * tags:
     *   name: Article APIs
     *   description: APIs to handle article.
     */

    /**
     * @swagger
     * /api/article-category/:
     *  get:
     *      description: Use to get All Category
     *      tags: [Article APIs]
     *      consumes:
     *        - application/json
     *      security:
     *        - bearerAuth: []
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.get("/",[authJwt.verifyToken],articleCategory.getAll);

    /**
     * @swagger
     * /api/article-category/{categoryId}:
     *  get:
     *      description: Use to get category by Id
     *      tags: [Article APIs]
     *      consumes:
     *        - application/json
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - name: categoryId
     *          in: path
     *          description: The id of the category
     *          required: true
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.get("/:categoryId",[authJwt.verifyToken],articleCategory.getById);

    /**
     * @swagger
     * tags:
     *   name: Article APIs
     *   description: APIs to handle article.
     */

    /**
     * @swagger
     * /api/article-category/create:
     *  post:
     *      description: Use to create category
     *      tags: [Article APIs]
     *      consumes:
     *        - application/json
     *      parameters:
     *        - in: body
     *          name: Create category
     *          description: Use to create category
     *          schema:
     *            type: object
     *            required:
     *              - title
     *            properties:
     *              title:
     *                type: string
     *      security:
     *        - bearerAuth: []
     *      responses:
     *          '200':
     *              description: A successful response
     *          '412':
     *              description: Error Validation
     *          '500':
     *              description: Internal server error
     */
    router.post("/create",[authJwt.verifyToken],articleCategory.create);

    app.use('/api/article-category',router);
};