const menu = require("../../controllers/base/menu.controller");
const { authJwt } = require("../../middleware");
module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    router.get("/",[authJwt.verifyToken],menu.getAll);
    router.get("/get",[authJwt.verifyToken],menu.getAllPagination);
    router.get("/get/:menuId",[authJwt.verifyToken],menu.getById);
    router.post("/create",[authJwt.verifyToken],menu.create);
    router.put("/update/:menuId",[authJwt.verifyToken],menu.update);
    router.delete("/delete/:menuId",[authJwt.verifyToken],menu.delete);

    app.use('/api/menu',router);
};