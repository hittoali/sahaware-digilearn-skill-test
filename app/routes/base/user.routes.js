const user = require("../../controllers/base/user.controller");
const { authJwt } = require("../../middleware");
module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    router.get("/",[authJwt.verifyToken],user.getAll);
    router.get("/get",[authJwt.verifyToken],user.getAllPagination);
    router.get("/get/:userId",[authJwt.verifyToken],user.getById);
    router.post("/create",[authJwt.verifyToken],user.create);
    router.put("/update/:userId",[authJwt.verifyToken],user.update);
    router.delete("/delete/:userId",[authJwt.verifyToken],user.delete);

    app.use('/api/user',router);
};