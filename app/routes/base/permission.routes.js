const permission = require("../../controllers/base/permission.controller");
const { authJwt } = require("../../middleware");
module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    router.get("/",[authJwt.verifyToken],permission.getAll);
    router.get("/get",[authJwt.verifyToken],permission.getAllPagination);
    router.get("/get/:permissionId",[authJwt.verifyToken],permission.getById);
    router.post("/create",[authJwt.verifyToken],permission.create);
    router.put("/update/:permissionId",[authJwt.verifyToken],permission.update);
    router.delete("/delete/:permissionId",[authJwt.verifyToken],permission.delete);

    app.use('/api/permission',router);
};