const role = require("../../controllers/base/role.controller");
const { authJwt } = require("../../middleware");
module.exports = app => {
    var router = require("express").Router();
    app.use(function(req, res, next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    router.get("/",[authJwt.verifyToken],role.getAll);
    router.get("/get",[authJwt.verifyToken],role.getAllPagination);
    router.get("/get/:roleId",[authJwt.verifyToken],role.getById);
    router.post("/create",[authJwt.verifyToken],role.create);
    router.put("/update/:roleId",[authJwt.verifyToken],role.update);
    router.delete("/delete/:roleId",[authJwt.verifyToken],role.delete);

    app.use('/api/role',router);
};