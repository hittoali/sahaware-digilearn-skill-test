const db = require("../models");
const article = db.article;
const Category = db.category;
const User = db.user;
const validator = require("../helpers/validate");
const getPagination = (page, size) => {
    page = page - 1;
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
  };
const getPagingData = (data, page, limit, size, offset) => {
  const { count: totalItems, rows: content } = data;
  const currentPage = page ? +page : 0;
  const totalPages = Math.ceil(totalItems / limit);
  const meta = {
      page : page,
      show : size,
      totalPage : totalPages,
      totalData : totalItems
  }
  return { totalItems, content, totalPages, currentPage, meta };
};
require('dotenv').config()

exports.create = (req,res) => {
    console.log(req.image);
    const validationRule = {
        "title": "required|string",
        "short_description": "required|string",
        "description": "required|string",
        "category_id": "required|integer"
    }

    async function createArticle() {
        try {
            const articleCreated = await article.create({
                title: req.body.title,
                short_description: req.body.short_description,
                description: req.body.description,
                category_id: req.body.category_id,
                is_visible: req.body.is_visible,
                image: process.env.IMAGE_PATH + req.file.filename,
                createdBy: req.userId
            });

            res.status(200).send({
                code: 200,
                success: true,
                message: "Article was created successfully!",
                data: articleCreated,
                totalItems: [articleCreated].length
            });

        } catch (error) {
            res.status(500).send({
                code: 500,
                success: false,
                message: error.message,
                data: null,
                totalItems: null
            });
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412).send({
                code: 412,
                success: false,
                message: 'Validation failed',
                data: err
            })
        } else {
            createArticle();
        }
    });
};

exports.getData = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    article.findAndCountAll({
            limit,
            offset, 
            include: [{
                model: Category,
                as: "category",
                attributes:['title']
            },{
                model: User,
                as: "created",
                attributes:['name']
            }]
        })
        .then((result) => {
            const response = getPagingData(result, page, limit, size, offset);
            res.status(200).send({
                code: 200,
                success: true,
                message: "Data found",
                data: response.content,
                meta: response.meta
            });
        }).catch((err) => {
            res.status(500).send({
                code: 500,
                success: false,
                message:
                    err.message || "Some error occurred while retrieving Article category",
                data: null,
                totalItems: null
            });
        });
}