const db = require("../models");
const config = require("../config/auth.config");
const validator = require("../helpers/validate");
const User = db.user;
const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
    const validationRule = {
        "name": "required|string",
        "email": "required|email|exist:user,email,null",
        "password": "required",
        "phone": "required|string|min:7"
    }

    async function createUser() {
        try {
            const userCreated = await User.create({
                name: req.body.name,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 8),
                phone: req.body.phone
            });

            res.status(200).send({
                code: 200,
                success: true,
                message: "User was registered successfully!",
                data: userCreated
            });

        } catch (error) {
            res.status(500).send({
                code: 500,
                success: false,
                message: error.message,
                data: null
            });
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412).send({
                code: 412,
                success: false,
                message: 'Validation failed',
                data: err
            })
        } else {
            createUser();
        }
    })
};

exports.signin = (req, res) => {
    const validationRule = {
        "email": "required|string",
        "password": "required",
    }

    async function loginUser() {
        try {
            const userLogin = await User.findOne({
                where: {
                    email: req.body.email
                }
            });

            if(!userLogin) {
                return res.status(404).send({
                    code: 404,
                    success: false,
                    message: 'User not found',
                    data: null
                });
            }
    
            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                userLogin.password
            );
    
            if(!passwordIsValid) {
                return res.status(401).send({
                    code: 401,
                    success: false,
                    message: "Invalid Password",
                    data: null
                });
            }
    
            var token = jwt.sign({
                id: userLogin.id
            }, config.secret,{
                expiresIn: 86400 //24 hours
            });

            userLogin.setDataValue('token', token);
    
            res.status(200).send({
                code: 200,
                success: true,
                message: 'Success login',
                data : userLogin
            });

        } catch (error) {
            res.status(500).send({
                code: 500,
                success: false,
                message: error.message,
                data: null
            });
        }
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412).send({
                code: 412,
                success: false,
                message: 'Validation failed',
                data: err
            })
        } else {
            loginUser();
        }
    })

    
};

exports.logout = (req, res) => {
    async function logout() {
        let authorization = req.headers.authorization;
        let token = authorization.split(" ")[1];

        try {
            await redisClient.LPUSH('token',token);
            res.status(200).send({
                code: 200,
                success: true,
                message: 'You are logged out',
                data : null
            });   
        } catch (error) {
            res.status(500).send({
                code: 500,
                success: false,
                message: error.message,
                data: null
            });
        }
    }

    logout();
}