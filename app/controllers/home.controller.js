exports.home = (req, res) => {
    res.status(200).send({
        appname: "Article API, Authentication, Swagger",
        author: "Ali Padilah"
    });
}