const db = require("../../models");
const menu = db.menu;
const validator = require("../../helpers/validate");
const { Sequelize } = require("../../models");
const Op = require('sequelize').Op;
const { getPagination, getPagingData, jsonResponse } = require("../../helpers/global");

exports.getAll = (req, res) => {
    menu.findAll({
            attributes: ['id','parent_id','name','icon','url','position','createdAt','createdBy'],
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving menu", null, null);
        });
}

exports.getAllPagination = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    menu.findAndCountAll({
            limit,
            offset,
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            const response = getPagingData(result, page, limit, size, offset);
            jsonResponse(res, 200, true, "Data found", response.content, response.meta);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving menu", null, null);
        });
}

exports.getById = (req, res) => {
    const menu_id = req.params.menuId;
    menu.findAll({
            attributes: ['id','parent_id','name','icon','url','position','createdAt','createdBy'],
            where: {id : menu_id, deletedAt: {[Op.eq]: null}}
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving menu", null, null);
        });
}

exports.create = (req,res) => {
    const validationRule = {
        "name": "required|string|exist:menu,name,null",
    }

    async function createmenu() {
        try {
            const menuCreated = await menu.create({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                parent_id: (req.body.parent_id) ? req.body.parent_id : null,
                icon: (req.body.icon) ? req.body.icon : null,
                url: (req.body.url) ? req.body.url : null,
                position: (req.body.position) ? req.body.position : null,
                createdBy: req.userId
            });

            jsonResponse(res, 200, true, "menu was created successfully!", menuCreated, [menuCreated].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            createmenu();
        }
    });
};

exports.update = (req,res) => {
    const menu_id = req.params.menuId;
    const validationRule = {
        "name": "required|string|exist:menu,name,"+menu_id,
    }

    async function updatemenu() {
        try {
            const menuUpdated = await menu.update({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                parent_id: (req.body.parent_id) ? req.body.parent_id : null,
                icon: (req.body.icon) ? req.body.icon : null,
                url: (req.body.url) ? req.body.url : null,
                position: (req.body.position) ? req.body.position : null,
                updatedBy: req.userId
            }, {
                where: {id: menu_id}
            });
            jsonResponse(res,200,true,"menu was updated successfully!",menuUpdated,[menuUpdated].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            updatemenu();
        }
    });
};

exports.delete = (req,res) => {
    const menu_id = req.params.menuId;
    async function deletemenu() {
        try {
            const menuDeleted = await menu.update({
                deletedBy: req.userId,
                deletedAt: Sequelize.NOW
            }, {
                where: {id: menu_id}
            });

            jsonResponse(res,200,true,"menu was deleted successfully!",menuDeleted,[menuDeleted].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    deletemenu();
};