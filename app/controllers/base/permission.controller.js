const db = require("../../models");
const permission = db.permission;
const validator = require("../../helpers/validate");
const { Sequelize } = require("../../models");
const Op = require('sequelize').Op;
const { getPagination, getPagingData, jsonResponse } = require("../../helpers/global");

exports.getAll = (req, res) => {
    permission.findAll({
            attributes: ['id','name','description','createdAt','createdBy'],
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving permission", null, null);
        });
}

exports.getAllPagination = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    permission.findAndCountAll({
            limit,
            offset,
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            const response = getPagingData(result, page, limit, size, offset);
            jsonResponse(res, 200, true, "Data found", response.content, response.meta);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving Permission", null, null);
        });
}

exports.getById = (req, res) => {
    const permission_id = req.params.permissionId;
    permission.findAll({
            attributes: ['id','name','description','createdAt','createdBy'], 
            where: {id : permission_id, deletedAt: {[Op.eq]: null}}
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving Permission", null, null);
        });
}

exports.create = (req,res) => {
    const validationRule = {
        "name": "required|string|exist:permission,name,null",
    }

    async function createPermission() {
        try {
            const permissionCreated = await permission.create({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                createdBy: req.userId
            });

            jsonResponse(res, 200, true, "Permission was created successfully!", permissionCreated, [permissionCreated].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            createPermission();
        }
    });
};

exports.update = (req,res) => {
    const permission_id = req.params.permissionId;
    const validationRule = {
        "name": "required|string|exist:permission,name,"+permission_id,
    }

    async function updatePermission() {
        try {
            const permissionUpdated = await permission.update({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                updatedBy: req.userId
            }, {
                where: {id: permission_id}
            });
            jsonResponse(res,200,true,"Permission was updated successfully!",permissionUpdated,[permissionUpdated].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            updatePermission();
        }
    });
};

exports.delete = (req,res) => {
    const permission_id = req.params.permissionId;
    async function deletePermission() {
        try {
            const permissionDeleted = await permission.update({
                deletedBy: req.userId,
                deletedAt: Sequelize.NOW
            }, {
                where: {id: permission_id}
            });

            jsonResponse(res,200,true,"Permission was deleted successfully!",permissionDeleted,[permissionDeleted].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    deletePermission();
};