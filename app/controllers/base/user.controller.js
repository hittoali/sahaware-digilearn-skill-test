const db = require("../../models");
const user = db.user;
const roleUser = db.roleUser;
const sequelize = db.sequelize;
const validator = require("../../helpers/validate");
const { Sequelize } = require("../../models");
const Op = require('sequelize').Op;
const { getPagination, getPagingData, jsonResponse } = require("../../helpers/global");
const bcrypt = require("bcryptjs");

exports.getAll = (req, res) => {
    user.findAll({
            attributes: ['id','name','email','phone','createdAt','createdBy'],
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving user", null, null);
        });
}

exports.getAllPagination = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    user.findAndCountAll({
            limit,
            offset,
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            const response = getPagingData(result, page, limit, size, offset);
            jsonResponse(res, 200, true, "Data found", response.content, response.meta);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving user", null, null);
        });
}

exports.getById = (req, res) => {
    const user_id = req.params.userId;
    user.findAll({
            attributes: ['id','name','email','phone','createdAt','createdBy'],
            where: {id : user_id, deletedAt: {[Op.eq]: null}}
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving user", null, null);
        });
}

exports.create = (req,res) => {
    const validationRule = {
        "name": "required|string|exist:user,name,null",
        "email": "required|email|exist:user,email,null",
        "password": "required|string|min:7"
    }

    async function createuser() {

        let transaction;
        try {
            transaction = await sequelize.transaction();
            
            const userCreated = await user.create({
                name: req.body.name,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 8),
                phone: (req.body.phone) ? req.body.phone : null,
                createdBy: req.userId
            }, { transaction });

            var mapping_roles_user = [];
            if (req.body.roles) {
                let roles = req.body.roles
                roles.forEach(element => {
                    mapping_roles_user.push({
                        user_id: userCreated.id,
                        role_id: element,
                        createdBy: req.userId
                    });
                });
                
                await roleUser.bulkCreate(mapping_roles_user, { transaction });
            }

            await transaction.commit();
            jsonResponse(res, 200, true, "user was created successfully!", userCreated, [userCreated].length);
        } catch (err) {
            if (transaction) {
                await transaction.rollback();
            }
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            createuser();
        }
    });
};

exports.update = (req,res) => {
    const user_id = req.params.userId;
    const validationRule = {
        "name": "required|string|exist:user,name,"+user_id,
        "email": "required|email|exist:user,email,"+user_id,
        "password": "string|min:7"
    }

    var data_update = data_update = {
        name: req.body.name,
        email: req.body.email,
        phone: (req.body.phone) ? req.body.phone : null,
        createdBy: req.userId
    };

    if (req.body.password) {
        let data_password = {
            password: bcrypt.hashSync(req.body.password, 8)
        };

        Object.assign(data_update,data_password);
    }

    let transaction;
    async function updateuser() {
        try {
            transaction = await sequelize.transaction();

            const userUpdated = await user.update(data_update, {
                where: { id: user_id }
            }, { transaction });

            var mapping_roles_user = [];
            if (req.body.roles) {
                let roles = req.body.roles
                roles.forEach(element => {
                    mapping_roles_user.push({
                        user_id: user_id,
                        role_id: element,
                        createdBy: req.userId
                    });
                });
                
                await roleUser.destroy({
                    where: { user_id: user_id }
                }, { transaction });

                await roleUser.bulkCreate(mapping_roles_user, { transaction });
            }

            await transaction.commit();
            jsonResponse(res,200,true,"user was updated successfully!",userUpdated,[userUpdated].length);
        } catch (err) {
            if (transaction) {
                await transaction.rollback();
            }
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            updateuser();
        }
    });
};

exports.delete = (req,res) => {
    const user_id = req.params.userId;
    async function deleteuser() {
        try {
            const userDeleted = await user.update({
                deletedBy: req.userId,
                deletedAt: Sequelize.NOW
            }, {
                where: {id: user_id}
            });

            jsonResponse(res,200,true,"user was deleted successfully!",userDeleted,[userDeleted].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    deleteuser();
};