const db = require("../../models");
const role = db.role;
const roleMenuPermission = db.roleMenuPermission;
const sequelize = db.sequelize;
const validator = require("../../helpers/validate");
const { Sequelize } = require("../../models");
const Op = require('sequelize').Op;
const { getPagination, getPagingData, jsonResponse } = require("../../helpers/global");

exports.getAll = (req, res) => {
    role.findAll({
            attributes: ['id','name','description','createdAt','createdBy'],
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving role", null, null);
        });
}

exports.getAllPagination = (req, res) => {
    const { page, size } = req.query;
    const { limit, offset } = getPagination(page, size);
    role.findAndCountAll({
            limit,
            offset,
            where : { deletedAt: {[Op.eq]: null} }
        })
        .then((result) => {
            const response = getPagingData(result, page, limit, size, offset);
            jsonResponse(res, 200, true, "Data found", response.content, response.meta);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving role", null, null);
        });
}

exports.getById = (req, res) => {
    const role_id = req.params.roleId;
    role.findAll({
            attributes: ['id','name','description','createdAt','createdBy'], 
            where: {id : role_id, deletedAt: {[Op.eq]: null}}
        })
        .then((result) => {
            jsonResponse(res, 200, true, "Data found", result, result.length);
        }).catch((err) => {
            jsonResponse(res, 500, false, err.message || "Some error occurred while retrieving role", null, null);
        });
}

exports.create = (req,res) => {
    const validationRule = {
        "name": "required|string|exist:role,name,null",
    }

    async function createRole() {

        let transaction;
        try {
            transaction = await sequelize.transaction();
            const roleCreated = await role.create({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                createdBy: req.userId
            }, { transaction });

            var mapping_role_menu = [];
            if (req.body.menus) {
                let menus = req.body.menus;
                menus.forEach(element => {
                    let permission = element.permissions;
                    let mapping_role_permission = [];
                    permission.forEach(el => {
                        mapping_role_permission.push(el);
                    });

                    mapping_role_menu.push({
                        role_id: roleCreated.id,
                        menu_id: element.menu_id,
                        permissions : mapping_role_permission,
                        createdBy: req.userId
                    });
                    mapping_role_permission = [];
                });

                await roleMenuPermission.bulkCreate(mapping_role_menu, { transaction });
            }

            await transaction.commit();
            jsonResponse(res, 200, true, "role was created successfully!", roleCreated, [roleCreated].length);
        } catch (err) {
            if (transaction) {
                await transaction.rollback();
            }
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            createRole();
        }
    });
};

exports.update = (req,res) => {
    const role_id = req.params.roleId;
    const validationRule = {
        "name": "required|string|exist:role,name,"+role_id,
    }

    async function updaterole() {
        let transaction;
        try {
            transaction = await sequelize.transaction();

            const roleUpdated = await role.update({
                name: req.body.name,
                description: (req.body.description) ? req.body.description : null,
                updatedBy: req.userId
            }, {
                where: {id: role_id}
            }, { transaction });

            var mapping_role_menu = [];
            if (req.body.menus) {
                let menus = req.body.menus;
                menus.forEach(element => {
                    let permission = element.permissions;
                    let mapping_role_permission = [];
                    permission.forEach(el => {
                        mapping_role_permission.push(el);
                    });

                    mapping_role_menu.push({
                        role_id: role_id,
                        menu_id: element.menu_id,
                        permissions : mapping_role_permission,
                        createdBy: req.userId
                    });
                    mapping_role_permission = [];
                });

                await roleMenuPermission.destroy({
                    where: { role_id: role_id }
                }, { transaction });

                await roleMenuPermission.bulkCreate(mapping_role_menu, { transaction });
            }

            await transaction.commit();
            jsonResponse(res,200,true,"role was updated successfully!",roleUpdated,[roleUpdated].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            jsonResponse(res, 412, false, 'Validation failed', err, null);
        } else {
            updaterole();
        }
    });
};

exports.delete = (req,res) => {
    const role_id = req.params.roleId;
    async function deleterole() {
        try {
            const roleDeleted = await role.update({
                deletedBy: req.userId,
                deletedAt: Sequelize.NOW
            }, {
                where: {id: role_id}
            });

            jsonResponse(res,200,true,"role was deleted successfully!",roleDeleted,[roleDeleted].length);
        } catch (err) {
            jsonResponse(res, 500, false, err.message, null, null);
        }
        
    }

    deleterole();
};