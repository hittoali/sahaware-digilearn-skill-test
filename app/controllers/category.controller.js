const db = require("../models");
const articleCategory = db.category;
const validator = require("../helpers/validate");

exports.create = (req,res) => {
    const validationRule = {
        "title": "required|string"
    }

    async function createCategory() {
        try {
            const categoryCreated = await articleCategory.create({
                title: req.body.title,
                createdBy: req.userId
            });

            res.status(200).send({
                code: 200,
                success: true,
                message: "Article category was created successfully!",
                data: categoryCreated,
                totalItems: [categoryCreated].length
            });

        } catch (error) {
            res.status(500).send({
                code: 500,
                success: false,
                message: error.message,
                data: null,
                totalItems: null
            });
        }
        
    }

    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412).send({
                code: 412,
                success: false,
                message: 'Validation failed',
                data: err
            })
        } else {
            createCategory();
        }
    });
};

exports.getAll = (req, res) => {
    articleCategory.findAll({attributes: ['id','title']})
        .then((result) => {
            res.status(200).send({
                code: 200,
                success: true,
                message: "Data found",
                data: result,
                totalItems: result.length
            });
        }).catch((err) => {
            res.status(500).send({
                code: 500,
                success: false,
                message:
                    err.message || "Some error occurred while retrieving Article category",
                data: null,
                totalItems: null
            });
        });
}

exports.getById = (req, res) => {
    const category_id = req.params.categoryId;
    articleCategory.findAll({attributes: ['id','title'], where: {id : category_id}})
        .then((result) => {
            res.status(200).send({
                code: 200,
                success: true,
                message: "Data found",
                data: result,
                totalItems: result.length
            });
        }).catch((err) => {
            res.status(500).send({
                code: 500,
                success: false,
                message:
                    err.message || "Some error occurred while retrieving Article category",
                data: null,
                totalItems: null
            });
        });
}