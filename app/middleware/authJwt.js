const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
const db = require("../models");
const redis = require("redis");
const { jsonResponse } = require("../helpers/global");

async function cekBlacklistToken(req,res,token) {    
    const result = await redisClient.LRANGE('token',0,99999999);
    if(result.indexOf(token) > -1){
        return res.status(401).send({
            code: 401,
            success: false,
            message: "Unauthorized!",
            data: null
        });
    }
}

verifyToken = (req,res,next) => {
    let authorization = req.headers.authorization;
    if(!authorization) {
        return res.status(403).send({
            code: 403,
            success: false,
            message: "No token provide",
            data: null
        });
    }

    let token = authorization.split(" ")[1];
    let actionReq = req.originalUrl.split("/");
    actionReq.shift();

    if (actionReq.length <= 2) {
        jsonResponse(res, 500, false, "Request not provide", null, null);
    } else {
        jsonResponse(res, 500, false, "Request not provide 2", actionReq.length, actionReq);
    }



    if(!token) {
        return res.status(403).send({
            code: 403,
            success: false,
            message: "No token provide",
            data: null
        });
    }

    // cekBlacklistToken(req,res,token);

    jwt.verify(token,config.secret,(err,decoded) => {
        if(err) {
            return res.status(401).send({
                code: 401,
                success: false,
                message: "Unauthorized!",
                data: null
            });
        }
        req.userId = decoded.id;
        next();
    });
};

const authJwt = {
    verifyToken: verifyToken
}

module.exports = authJwt;