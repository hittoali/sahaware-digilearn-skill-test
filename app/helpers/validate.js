const Validator = require('validatorjs');
const Models = require('../models');
const Op = require('sequelize').Op;

const validator = (body, rules, customMessages, callback) => {
    const validation = new Validator(body, rules, customMessages);
    validation.passes(() => callback(null, true));
    validation.fails(() => callback(validation.errors, false));
};

Validator.registerAsync('exist', function(value,  attribute, req, passes) {
    if (!attribute) throw new Error('Specify Requirements i.e fieldName: exist:table,column');
    //split table and column
    let attArr = attribute.split(",");
    if (attArr.length !== 3) throw new Error(`Invalid format for validation rule on ${attribute}`);

    //assign array index 0 and 1 to table and column respectively
    const { 0: table, 1: column, 2: id } = attArr;
    var filter = null;

    if(id !== 'null'){
        filter = {
            where: {
                [column]: value,
                id : {[Op.not]:id}
            }
        }
    } else {
        filter = {
            where: {
                [column]: value
            }
        }
    }

    //define custom error message
    let msg = `${column} has already been taken `;

    console.log(table,column,id);

    //check if incoming value already exists in the database
    Models[table].findOne(filter)
    .then((result) => {
        if(result){
            passes(false, msg); // return false if value exists
            return;
        }
        passes();
    })
});

module.exports = validator;