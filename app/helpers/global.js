const getPagination = (page, size) => {
    page = parseInt(page);
    size = parseInt(size);
    page = page - 1;
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
    return { limit, offset };
  };

const getPagingData = (data, page, limit, size, offset) => {
    page = parseInt(page);
    limit = parseInt(limit);
    size = parseInt(size);
    offset = parseInt(offset);

    const { count: totalItems, rows: content } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    let next = null;
    let previous = null;

    if (endIndex < totalItems) {
        next = {
            page: page + 1,
            size: limit
        };
    }

    if (startIndex > 0) {
        previous = {
            page: page - 1,
            size: limit
        };
    }

    const meta = {
        page : page,
        show : size,
        totalPage : totalPages,
        totalData : totalItems,
        next : next,
        previous : previous
    }
  return { totalItems, content, totalPages, currentPage, meta };
};

const jsonResponse = (res, code, booleanSuccess, message, data, meta) => {
    res.status(code).send({
        code: code,
        success: booleanSuccess,
        message: message,
        data: data,
        meta: meta
    });
}

module.exports = {
    getPagination,
    getPagingData,
    jsonResponse
};